package com.nespresso.exercise.electric_trip;

import java.util.concurrent.atomic.AtomicInteger;

public class Participant {

    static AtomicInteger counter = new AtomicInteger();

    private final Integer id = counter.incrementAndGet();
    private final int batterySize;
    private double remainingBattery;
    private final int lowSpeedPerformance;
    private final int highSpeedPerformance;
    private String location;

    public Participant(String startingFrom, int batterySize, int lowSpeedPerformance, int highSpeedPerformance) {
        this.location = startingFrom;
        this.batterySize = batterySize;
        this.remainingBattery =batterySize;
        this.lowSpeedPerformance = lowSpeedPerformance;
        this.highSpeedPerformance = highSpeedPerformance;
    }

    public static AtomicInteger getCounter() {
        return counter;
    }

    public Integer getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }

    public String getChargePercentage() {
        return ""+Math.round((100.0 * remainingBattery)/batterySize)+"%";
    }

    public void moveFastOn(TripMap map) {
        moveWithBatteryPerformance(map, highSpeedPerformance);
    }

    public void moveOn(TripMap map) {
        moveWithBatteryPerformance(map, lowSpeedPerformance);
    }

    private void moveWithBatteryPerformance(TripMap map, int batteryPerformance) {
        int totalAutonomy = batteryPerformance *(int)remainingBattery;
        int kilometersToDriveOn = map.remainingKilometers(location);
        if(canIReachTheEnd(totalAutonomy, kilometersToDriveOn)) {
            this.location = map.getDestination();
            updateBattery(kilometersToDriveOn,batteryPerformance);
        } else {
            String fartestCity = map.getFartestPointWithABattery(location,totalAutonomy);
            kilometersToDriveOn = map.remainingKilometers(location,fartestCity);
            this.location = fartestCity;
            updateBattery(kilometersToDriveOn,batteryPerformance);
        }
    }

    private boolean canIReachTheEnd(int totalAutonomy, int kilometersToDriveOn) {
        return totalAutonomy>=kilometersToDriveOn;
    }

    private void updateBattery(int remainingKilometers, int batteryPerformance) {
        this.remainingBattery-=(1.0*remainingKilometers)/batteryPerformance;
    }

    public void charge(TripMap map, int hoursOfCharge) {
        int hourlyCharge = map.getHourlyCharge(this.location);
        int totalMaxCharge = hoursOfCharge*hourlyCharge;
        this.remainingBattery = Math.min(batterySize,this.remainingBattery+totalMaxCharge);
    }
}
