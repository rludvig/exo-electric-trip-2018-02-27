package com.nespresso.exercise.electric_trip;

import java.util.LinkedList;
import java.util.List;

public class TripMap {

    private final List<Integer> chargesPerCity;
    private List<Integer> distancesBetweenCities;
    private List<String> cities;
    private final String destination;


    public TripMap(String destination, List<Integer> distancesBetweenCities, List<String> cities, List<Integer> chargesPerCity) {
        this.destination = destination;
        this.distancesBetweenCities = distancesBetweenCities;
        this.chargesPerCity = chargesPerCity;
        this.cities = cities;
    }

    public int remainingKilometers(String from) {
        int cityIndex = getCityIndex(from);
        int totalDistance = 0;
        for (int i = cityIndex; i < distancesBetweenCities.size(); i++) {
            totalDistance += distancesBetweenCities.get(i);
        }
        return totalDistance;
    }

    public static TripMap fromTripDetails(String tripDetails) {
        String[] tripDetailsTokens = tripDetails.split("-");
        String destination = tripDetailsTokens[tripDetailsTokens.length - 1];

        List<String> cities = new LinkedList();
        List<Integer> distances = new LinkedList<Integer>();
        List<Integer> charges = new LinkedList<Integer>();

        for (int i = 0; i < tripDetailsTokens.length; i++) {
            String token = tripDetailsTokens[i];
            if (isACityToken(i)) {
                registerCityName(cities, extractCityFromToken(token));
                registerCharge(charges, extractChargeFromToken(token));
            } else {
                registerDistance(distances, Integer.valueOf(token));
            }
        }
        return new TripMap(destination, distances, cities, charges);
    }

    private static void registerCityName(List<String> cities, String city) {
        cities.add(city);
    }

    private static void registerCharge(List<Integer> charges, Integer charge) {
        charges.add(charge);
    }

    private static Integer extractChargeFromToken(String token) {
        if (isCityWhereYouCanChargeTheCar(token)) {
            return Integer.valueOf(token.split(":")[1]);
        }
        return 0;
    }

    private static String extractCityFromToken(String token) {
        if (isCityWhereYouCanChargeTheCar(token)) {
            return token.split(":")[0];
        }
        return token;
    }

    private static boolean isCityWhereYouCanChargeTheCar(String token) {
        return token.contains(":");
    }

    private static void registerDistance(List<Integer> distances, Integer e) {
        distances.add(e);
    }

    private static boolean isACityToken(int i) {
        return i % 2 == 0;
    }

    public String getDestination() {
        return destination;
    }

    public String getFartestPointWithABattery(String startingFrom, int totalAutonomy) {
        String fartestPoint = getFartestPoint(startingFrom, totalAutonomy);
        int cityIndex = getCityIndex(fartestPoint);
        for(int i=cityIndex;i>0;i--) {
            if(chargesPerCity.get(i)>0) {
                return cities.get(i);
            }
        }
        return fartestPoint;
    }



    public String getFartestPoint(String startingFrom, int totalAutonomy) {
        int indexCurrentCity = getCityIndex(startingFrom);
        int leftAutonomy = totalAutonomy;
        for (; indexCurrentCity < distancesBetweenCities.size(); indexCurrentCity++) {
            Integer distanceToTheNextCity = distancesBetweenCities.get(indexCurrentCity);
            if (leftAutonomy < distanceToTheNextCity) {
                break;
            } else {
                leftAutonomy -= distanceToTheNextCity;
            }
        }
        return cities.get(indexCurrentCity);
    }

    public int remainingKilometers(String startingFrom, String fartestCity) {
        int indexStartingFrom = getCityIndex(startingFrom);
        int indexFartestCity = getCityIndex(fartestCity);
        int distance = 0;
        for (int i = indexStartingFrom; i < indexFartestCity; i++) {
            distance += distancesBetweenCities.get(i);
        }
        return distance;
    }

    private int getCityIndex(String startingFrom) {
        return this.cities.indexOf(startingFrom);
    }

    public int getHourlyCharge(String location) {
        return chargesPerCity.get(getCityIndex(location));
    }
}
