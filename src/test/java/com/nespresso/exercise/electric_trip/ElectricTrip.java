package com.nespresso.exercise.electric_trip;

import java.util.HashMap;
import java.util.Map;

public class ElectricTrip {


    Map<Integer,Participant> participantsMap = new HashMap<Integer, Participant>();
    TripMap map;

    public ElectricTrip(String tripDetails) {
        map = TripMap.fromTripDetails(tripDetails);
    }

    public int startTripIn(String startingFrom,
                           int batterySize,
                           int lowSpeedPerformance,
                           int highSpeedPerformance) {

        Participant participant = new Participant(startingFrom,batterySize,lowSpeedPerformance,highSpeedPerformance);
        participantsMap.put(participant.getId(),participant);
        return participant.getId();
    }

    public void go(int participantId) {
        Participant participant = getParticipant(participantId);
        participant.moveOn(map);
    }

    public void sprint(int participantId) {
        Participant participant = getParticipant(participantId);
        participant.moveFastOn(map);
    }

    private Participant getParticipant(int participantId) {
        return participantsMap.get(participantId);
    }

    public String locationOf(int participantId) {
        return getParticipant(participantId).getLocation();
    }

    public String chargeOf(int participantId) {
        return getParticipant(participantId).getChargePercentage();
    }


    public void charge(int participantId, int hoursOfCharge) {
        getParticipant(participantId).charge(map,hoursOfCharge);
    }
}
